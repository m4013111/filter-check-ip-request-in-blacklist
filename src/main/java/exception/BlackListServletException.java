package exception;

import javax.servlet.ServletException;

public class BlackListServletException extends ServletException {
    public BlackListServletException(String message) {
        super(message);
    }
}
