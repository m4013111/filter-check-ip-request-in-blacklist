package repo;

import lombok.RequiredArgsConstructor;

import java.io.*;
import java.nio.file.*;
import java.util.Properties;
import java.util.Set;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;


@RequiredArgsConstructor
public class FileListIpRepository implements ListIpRepository {
    // private final String path;  //path to file with blacklist
    private final File blackListFile;
    private final Set<String> setIp;

    @Override
    public boolean isPresentIp(String ip) {
       // updateList();
      return setIp.contains(ip);

    }



    @Override
    public void updateList() {
        setIp.clear();
        FileReader fr = null;
        try {
            fr = new FileReader(blackListFile);
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        //создаем BufferedReader с существующего FileReader для построчного считывания
        BufferedReader reader = new BufferedReader(fr);
        // считаем сначала первую строку
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        do {
            System.out.println(line);
            // считываем остальные строки в цикле
            try {

                setIp.add(line.trim());
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (line != null);
      }

    @Override
    public void testFileUpWather() throws IOException {

        Path path = Paths.get(blackListFile.getParent());
        WatchService watcher = FileSystems.getDefault().newWatchService();
        path.register(watcher, ENTRY_MODIFY);

        new Thread(() -> {
            try {
                while (true) {
                    WatchKey key = watcher.take();
                    for (WatchEvent<?> event : key.pollEvents()) {
                        if (event.kind() == StandardWatchEventKinds.OVERFLOW) {
                            // Событие может быть потеряно или отменено
                            continue;
                        }
                        Path fileName = (Path) event.context();
                        System.out.println("Обновление файла:" + fileName);
                        updateList();
                    }
                    if (!key.reset()) { // сбросить WatchKey
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();


        try {
            Thread.sleep(1000 );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
