package repo;

import java.io.IOException;

public interface ListIpRepository {
     boolean isPresentIp(String ip); //check whether ip belongs to blacklist or not
     void updateList(); //update blacklist from repository
     void testFileUpWather() throws IOException;

 }
