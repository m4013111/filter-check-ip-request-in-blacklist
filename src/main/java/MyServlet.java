import filter.IpFilter2;
import repo.FileListIpRepository;
import repo.ListIpRepository;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;


public class MyServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getOutputStream().write("Access allowed!".getBytes());
        resp.getOutputStream().flush();
    }

    @Override
    public void init() throws ServletException {
        ListIpRepository listIp;
        System.out.println("init servlet");
        File file = new File(getFilePath());
        Set<String> setIp = new HashSet();

        listIp = new FileListIpRepository(file,setIp);
        listIp.updateList();
        getServletContext().setAttribute("listIp",listIp);
        try {
            listIp.testFileUpWather();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFilePath() {
        Properties properties = new Properties();
        try {
            //   ClassLoader loader = Thread.currentThread().getContextClassLoader();
            //    properties.load(IpFilter2.class.getClassLoader().getResourceAsStream("application.properties"));
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
            String path = properties.getProperty("path");
            return path;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }



}
