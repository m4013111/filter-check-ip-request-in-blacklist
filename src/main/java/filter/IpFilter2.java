package filter;

import exception.BlackListServletException;
import repo.FileListIpRepository;
import repo.ListIpRepository;

import javax.servlet.*;
import javax.servlet.Filter;
import java.io.File;
import java.io.IOException;
import java.util.Properties;




public class IpFilter2 implements Filter {
   // ListIpRepository listIp;
    ListIpRepository listIp;
    FilterConfig filterConfig;




    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("InitFilter!");
        this.filterConfig = filterConfig;


    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("doFilter!");
        String curIp = servletRequest.getRemoteAddr();
        listIp = (ListIpRepository) this.filterConfig.getServletContext().getAttribute("listIp");
        if (listIp.isPresentIp(curIp)){
            System.out.println("IP "+curIp+ " is in black list!");
            throw new BlackListServletException("IP "+curIp+ " is in black list!");
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("FilterDestroy!");
    }
}
